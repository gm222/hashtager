﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace HashTager.Frames
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class Create : HashTager.Common.LayoutAwarePage
    {
        private Szyfrowanie dane;
        private Windows.Storage.StorageFile file;
        public Create()
        {
            this.InitializeComponent();

            List<string> hashList = new List<string>
            {
                "Md5",
                "Sha1",
                "Sha256",
                "Sha384",
                "Sha512"
            };

            foreach (string hash in hashList)
            {
                var lista   =   new ComboBoxItem()
                {
                    Content =   hash
                };

                this._hashtyp.Items.Add(lista);
            }
        }

        private async void Wygeneruj(object sender, RoutedEventArgs e)
        {
            if (this._hashtyp.SelectedIndex < 0)
            {
                var wiad = new Windows.UI.Popups.MessageDialog("Wybierz algorytm szyfrujący", "Błąd");
                await wiad.ShowAsync();
                return;
            }
            var algorytm = this._hashtyp.SelectionBoxItem.ToString();

            dane = new Szyfrowanie(file, algorytm, this._text.Text);

            this._plikZakodowany.Text = await dane.SzyfrowaniePliku();
            this._textZakodowany.Text = dane.SzyfrowanieTekstu();
        }

        private async void WybierzPlik(object sender, RoutedEventArgs e)
        {
            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Udziel dostępu do plików.", "Informacja")).ShowAsync();
                return;
            }
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.FileTypeFilter.Add("*");
            openPicker.SuggestedStartLocation = PickerLocationId.ComputerFolder;

            file = await openPicker.PickSingleFileAsync();

            if (file != null)
                this._plik.Text = file.Path;
        }
    }
}
