﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace HashTager.Frames
{
    public sealed partial class Check : HashTager.Common.LayoutAwarePage
    {
        private Szyfrowanie dane;
        private Windows.Storage.StorageFile file;

        public Check()
        {
            this.InitializeComponent();
            List<string> hashList = new List<string>
            {
                "Md5",
                "Sha1",
                "Sha256",
                "Sha384",
                "Sha512"
            };

            foreach (string hash in hashList)
            {
                var lista   =   new ComboBoxItem()
                {
                    Content =   hash
                };

                this._hashtyp.Items.Add(lista);
            }
            this._hashtyp.SelectedIndex = 0;
        }

        private async void WybierzPlik(object sender, RoutedEventArgs e)
        {
            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Udziel dostępu do plików.", "Informacja")).ShowAsync();
                return;
            }
            FileOpenPicker openPicker           =       new FileOpenPicker();
            openPicker.ViewMode                 =       PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation   =       PickerLocationId.ComputerFolder;
            openPicker.FileTypeFilter.Add("*");

            Windows.Storage.StorageFile file2   =       await openPicker.PickSingleFileAsync();

            if (file2 != null)
            {
                file                =   file2;
               
                this._plik.Text     =   file.Path;
                if (this._hashtyp.SelectedIndex < 0)
                    return;

                var algorytm        =   this._hashtyp.SelectionBoxItem.ToString();
                dane                =   new Szyfrowanie(file, algorytm, "");
                this._text.Text     =   await dane.SzyfrowaniePliku();
            }
        }

        private async void Sprawdz(object sender, RoutedEventArgs e)
        {
            if (this._hashtyp.SelectedIndex < 0)
            {
                var wiad = new Windows.UI.Popups.MessageDialog("Wybierz algorytm szyfrujący", "Błąd");
                await wiad.ShowAsync();
                return;
            }
            var algorytm = this._hashtyp.SelectionBoxItem.ToString();

            dane = new Szyfrowanie(file, algorytm, "");
            string hasplik = await dane.SzyfrowaniePliku();
            this._text.Text = hasplik;

            if (hasplik.ToLower() == this._textP.Text.ToLower())
                await (new Windows.UI.Popups.MessageDialog("Podane hashe są identyczne. Suma kontrolna się zgadza", "Informacja")).ShowAsync();
            else
                await (new Windows.UI.Popups.MessageDialog("Podane hashe są różne. Suma kontrolna jest różna od podanego hashu", "Informacja")).ShowAsync();
        }

        private async void _hashtyp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this._hashtyp.SelectedIndex < 0)
                return;
            try
            {
                var algorytm        =   (this._hashtyp.SelectedItem as ComboBoxItem).Content.ToString();
                dane                =   new Szyfrowanie(file, algorytm, "");
                string hasplik      =   await dane.SzyfrowaniePliku();
                this._text.Text     =   hasplik;
            }
            catch (Exception) { };
        }
  
    }
}
