﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;

namespace HashTager
{
    public class Szyfrowanie
    {

        private StorageFile file;
        private string algorytm;
        private string text;

        public Szyfrowanie(StorageFile _file, string _alg, string _text)
        {
            this.file = _file;
            this.algorytm = _alg;
            this.text = _text;
        }

        public async Task<string> SzyfrowaniePliku()
        {
            if (file != null)
                return await ZaszyfryjPlik(file, algorytm);
            return "";
        }

        public string SzyfrowanieTekstu()
        {
            if (text.Length > 0)
                return ZaszyfrujText(algorytm, this.text);
            return "";
        }

        private async Task<string> ZaszyfryjPlik(StorageFile file, string alg) //dla szyfrowania pliku
        {
            var stream = await file.OpenStreamForReadAsync();
            var algorytm = HashAlgorithmProvider.OpenAlgorithm(alg.ToUpper());
            var inputStream = stream.AsInputStream();
            uint capacity = 100000000; //ograniczenie ładowania
            var buffer = new Windows.Storage.Streams.Buffer(capacity);
            var hash = algorytm.CreateHash();  //generowanie hasu ze bufforu

            // w przyszłości AES, RC i inne
            // zrobić poprzez wybór switch providera

            while (true) //dopuki buffer nie będzie <= 0
            {
                await inputStream.ReadAsync(buffer, capacity, InputStreamOptions.None);
                if (buffer.Length > 0)
                    hash.Append(buffer);
                else
                    break;
            }

            //zwolnienie strumieni
            inputStream.Dispose();
            stream.Dispose();

            return CryptographicBuffer.EncodeToHexString(hash.GetValueAndReset()).ToUpper();
        }

        private string ZaszyfrujText(string alg, string wiad) //dla szyfrowania textu
        {
            var algorytm = HashAlgorithmProvider.OpenAlgorithm(alg.ToUpper());
            var buffUtf8Msg = CryptographicBuffer.ConvertStringToBinary(wiad,
                                                                        BinaryStringEncoding.Utf8); //konwersja do binarnego rozkładu z użyciem UTF8
            var buffHash = algorytm.HashData(buffUtf8Msg);

            if (buffHash.Length != algorytm.HashLength)
                return "Wystąpił nieoczekiwany błąd w tworzeniu hashu";

            return CryptographicBuffer.EncodeToHexString(buffHash);
        }

    }
}
