﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace HashTager
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : HashTager.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
            #region
            /*
            var komunikat = new TextBlock()
            {
                 Text                   =   "Wybierz jedną z opcji",
                 HorizontalAlignment    =   Windows.UI.Xaml.HorizontalAlignment.Center,
                 VerticalAlignment      =   Windows.UI.Xaml.VerticalAlignment.Center,
                 Foreground             =   new SolidColorBrush(Windows.UI.Color.FromArgb(255,255,0,0)),
                 FontFamily             =   new Windows.UI.Xaml.Media.FontFamily("Segoe UI"),
                 FontSize               =   58 
            };

            zawartosc.Content               =   komunikat;
             * */
            #endregion
        }

        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            zawartosc.Content = null;
            zawartosc.Content = new HashTager.Frames.Create();
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            zawartosc.Content = null;
            zawartosc.Content = new HashTager.Frames.Check();
        }
    }
}
