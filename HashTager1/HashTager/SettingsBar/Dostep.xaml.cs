﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HashTager.SettingsBar
{
    public sealed partial class Dostep : UserControl
    {
        public Dostep()
        {
            this.InitializeComponent();
            this._switch.IsOn = App._dostep;
        }

        private void _switch_Toggled(object sender, RoutedEventArgs e)
        {
            App._dostep = this._switch.IsOn;
            SaveData();
        }

        private async void SaveData()
        {
            StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Dane",
            CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);
            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                bool dos = App._dostep;
                DataContractSerializer serializer = new DataContractSerializer(typeof(System.Boolean));
                serializer.WriteObject(outStream.AsStreamForWrite(), dos);
                await outStream.FlushAsync();
            }
            raStream.Dispose();
        }
    }
}
