﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Callisto.Controls;
using Windows.UI.ApplicationSettings;
using Windows.System;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace HashTager
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        public static bool _dostep;

        public App()
        {
            this.InitializeComponent();

            _dostep = false;
            this.Suspending += OnSuspending;
        }

        private async void Pytanie()
        {
            var pyt = new Windows.UI.Popups.MessageDialog("Allow this application to access files and folders ?", "Question");
            pyt.Commands.Add(new UICommand("Allow", new UICommandInvokedHandler(Zmiana)));
            pyt.Commands.Add(new UICommand("Decline", new UICommandInvokedHandler(Zmiana)));

            pyt.DefaultCommandIndex = 0;
            pyt.CancelCommandIndex = 1;

            await pyt.ShowAsync();

            SaveData();

        }

        private async void SaveData()
        {
            StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Dane",
                                                                CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);
            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                bool jakidostep = App._dostep;
                DataContractSerializer serializer = new DataContractSerializer(typeof(System.Boolean));

                serializer.WriteObject(outStream.AsStreamForWrite(), jakidostep);
                await outStream.FlushAsync();
            }
            raStream.Dispose();
        }

        private void Zmiana(Windows.UI.Popups.IUICommand command)
        {
            switch (command.Label)
            {
                case "Allow":
                    _dostep = true;
                    break;
                case "Decline":
                    _dostep = false;
                    break;
            }
        }

        private async void Open()
        {
            StorageFile file = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("Dane");
            }
            catch (Exception) { };

            if (file == null)
            {
                Pytanie();
                return;
            }

            IRandomAccessStream inStream = await file.OpenReadAsync();
            DataContractSerializer serializer = new DataContractSerializer(typeof(System.Boolean));            // Deserialize the Session State.
            _dostep = (bool)serializer.ReadObject(inStream.AsStreamForRead());

            inStream.Dispose();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                Open();
                SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;
                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }


        private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            //Zmiana dostępu do plików
            var pliki_dostep = new SettingsCommand("pliki", "File Access", (handler) =>
            {
                var settings = new Callisto.Controls.SettingsFlyout()
                {
                    ContentBackgroundBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0)),
                    Content = new SettingsBar.Dostep(),
                    HeaderText = "Pliki",
                    IsOpen = true
                };
            });

            //Polityka Prywatności
            var privacy = new SettingsCommand("privacypolicy", "Privacy Policy", OpenPrivacyPolicy);
            //Ocena i Komentarze
            var ocen = new SettingsCommand("ocenkomentarz", "Rate / Comment", OpenReviewComment);

            args.Request.ApplicationCommands.Add(pliki_dostep);
            args.Request.ApplicationCommands.Add(privacy);
            args.Request.ApplicationCommands.Add(ocen);

        }

        private async void OpenReviewComment(IUICommand command)
        {
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=5623.gm222.HashTager_pg6tpmnpf1jnm"));
        }

        private async void OpenPrivacyPolicy(IUICommand command)
        {
            var uri = new Uri("http://student.math.uni.opole.pl/~id11130/PolitykaPrywatnosci/PolitykaPrywatności1.pdf");
            await Launcher.LaunchUriAsync(uri);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }
}
